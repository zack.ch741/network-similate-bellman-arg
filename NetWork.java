
package simple;

import java.util.ArrayList;
import java.util.Random;

public class NetWork {
	private ArrayList<NetNode>nodes;
	int terminal;
	int netsize;
	
	Protocol configer;
	
	NetWork(int size){
		nodes = new ArrayList<NetNode>();
		this.netsize = size;
		//set up start node
		
		for(int i =0;i <size;i++)
		{
			nodes.add(new NetNode(i));
		}
		terminal = nodes.size()-1;
		
		sample();
		//ramdom_net();
		configer = new Protocol(this,0,6);
		/*for(int y = 0;y<10;y++){
			System.out.println();
		System.out.println("Cycle " + y);
		System.out.println();
		for(int i = 0;i<netsize;i++)
		{	
			
			node(i).print_node();
		}
		}*/
	}
	private void sample()
	{
	node(0).set_node_connection(node(0), 0);
		node(0).set_node_connection(node(1), 1);
		node(0).set_node_connection(node(2), 1);
		node(0).set_node_connection(node(4), 1);
		
		node(1).set_node_connection(node(0), 1);
	node(1).set_node_connection(node(1), 0);
		node(1).set_node_connection(node(4), 1);
		node(1).set_node_connection(node(5), 1);
		
		node(2).set_node_connection(node(0), 1);
	node(2).set_node_connection(node(2), 0);
		node(2).set_node_connection(node(3), 1);
		
		node(3).set_node_connection(node(2), 1);
	node(3).set_node_connection(node(3), 0);
		node(3).set_node_connection(node(4), 1);
		node(3).set_node_connection(node(5), 1);
		node(3).set_node_connection(node(6), 1);
		
		node(4).set_node_connection(node(0), 1);
		node(4).set_node_connection(node(1), 1);
		node(4).set_node_connection(node(3), 1);
	node(4).set_node_connection(node(4), 0);
		node(4).set_node_connection(node(5), 1);
		
		node(5).set_node_connection(node(1), 1);
		node(5).set_node_connection(node(3), 1);
		node(5).set_node_connection(node(4), 1);
	node(5).set_node_connection(node(5), 0);
		node(5).set_node_connection(node(6), 1);

		node(6).set_node_connection(node(3), 1);
		node(6).set_node_connection(node(5), 1);
	node(6).set_node_connection(node(6), 0);
	
	}
	public void ramdom_net()
	{
		
		int[]random = {1,2,3,4,5,6,7,8,9,0,9};
		ArrayList<Integer>seeds = new ArrayList<Integer>();
		boolean ca_add = true;
		Random rand = new Random();
		
		for(int y = 0; y <netsize;y++)
		{
			for(int i:random)
			{
				seeds.add(i);
			}
			node(y).set_node_connection(node(y), 0);
			for(int x = 0; x<4;x++)
			{
				int n = rand.nextInt(seeds.size()-1);
				
				if(seeds.get(n)!=y && seeds.get(n)<netsize)
				{
					for(int i =0;i<node(y).get_connection().size();i++)
					{
						if(node(y).get_connection().get(i).getTargetOrder() == seeds.get(n))
						{
							ca_add=false;
						}
					}
					if(ca_add==true)
					{
					node(y).set_node_connection(node(seeds.get(n)), 1);
					
					node(seeds.get(n)).set_node_connection(node(y), 1);
					seeds.remove(n);
					}
					else
					{
						ca_add=true;
					}
				}
			}
		}
		
		sort();

	}
	public void sort()
	{
		for(int i =0;i<netsize;i++)
		{
		
			for(int x = node(i).get_connection().size()-1; x>=0;x--)
				for(int y = 0; y<node(i).get_connection().size()-1;y++){
					Connection curr = node(i).get_connection().get(y);
				
					Connection next = node(i).get_connection().get(y+1);
				
					if(curr.getTargetOrder()>next.getTargetOrder())
					{
						
						Connection temp = new Connection(curr.getTargetOrder(),curr.getDistance(),curr.getVia());
					
						node(i).get_connection().get(y).setconnet(next);
						node(i).get_connection().get(y+1).setconnet(temp);
						
					}
				}
		}
	}
	public int getNetsize()
	{
		return netsize;
	}
		
	public NetNode node(int index)
	{
		return nodes.get(index);
	}

	public static void main(String[] args)
	{
		NetWork n = new NetWork(7);
	//n.printreasult();
	}
}
