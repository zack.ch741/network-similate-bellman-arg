package simple;

public class Connection {
	private int targetOrder;
	private int distance;
	private int Via;
	Connection(int targetOrder, int distance,int Via){
		this.targetOrder = targetOrder;
		this.distance = distance;
		this.Via = Via;
	}
	public int getTargetOrder() {
		return targetOrder;
	}
	public void setvia(int Via)
	{
		this.Via = Via;
	}
	public int getVia()
	{
		return Via;
	}
	public int getDistance() {
		return distance;
	}
	public void setOrder(int neworder){
		this.targetOrder = neworder;
	}
	public void setconnet(Connection newc){
		this.targetOrder = newc.getTargetOrder();
		this.distance = newc.getDistance();
		this.Via = newc.getVia();
	}
}
