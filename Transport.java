package simple;

public class Transport {
	int finalnode;
		Transport(Protocol proto,NetWork net)
		{
			this.finalnode=0;
			initial_update(proto,net);
		}
		
		private void initial_update(Protocol proto,NetWork net)
		{
			
			int cycel=1;
			
			System.out.println("Cycle 0");
			for(int i = 0;i<net.getNetsize();i++)
			{	
			
				net.node(i).print_node();
			}
			System.out.println();
			int loopc=1;
		
			for(int i = 0;i<proto.get_rout().size()-1;i++)
			{
				loopc++;
				finalnode=0;
				cycel+=i;
				System.out.println("Cycle " + (i+1));
				proto.get_topo().print(net);
				int current = proto.get_rout().get(i);
				int next = proto.get_rout().get(i+1);
				finalnode+=current;
				first_shack(current,next,net);
				System.out.println("via ->: "+finalnode);
				System.out.println();
			}
			System.out.println();
			System.out.println("Update stops after cycle: "+cycel+" last node via: " + finalnode);
			System.out.println();
			for(int cyc =loopc;cyc<10;cyc++)
			{
				loopc++;
				finalnode=0;
				cycel+=cyc;
				System.out.println("Cycle " + (cyc+1));
				proto.get_topo().print(net);
				
				//System.out.println("via ->: "+finalnode);
				System.out.println("Update stops");
				System.out.println();
			}
		
		
			
		}
		public int getVia()
		{
			return finalnode;
		}
		private boolean first_shack(int from, int terminate,NetWork net)
		{
			if(net.node(terminate).isavailible())
			{
				//System.out.println("first handshack");
				second_shack( from,  terminate, net);
			}
			return false;
		}
		private boolean second_shack(int from, int terminate,NetWork net)
		{
			
				transfer(from,  terminate, net);
				net.node(terminate).confirmation();
				if(third_shack( from,  terminate, net));
				{
				
					//System.out.println("UPDATE: transfered from node " + Integer.toString(from) + " to node " + Integer.toString(terminate));
				
					//System.out.println("second handshack");
				}
			
			return false;
		}
		private void transfer(int from, int terminate,NetWork net)
		{
			int temp =net.node(from).getDataholder();
			net.node(from).setDataholder(-temp);
			net.node(terminate).setDataholder(temp);
		}
		private boolean third_shack(int from, int terminate,NetWork net)
		{
			if(net.node(terminate).isconfirmed())
			{
				//System.out.println("third handshack");
				net.node(terminate).confirmation();
				return true;
			}
			return false;
		}
	
		
}
