package simple;

import java.util.ArrayList;

public class Protocol {
	private int start ;
	private int terminal ;
	private int netsize;
	private int[][] topology ;
	private ArrayList<Integer>rout;
	
	private topology Nettopo;
	private Transport transfer;
	Protocol(NetWork net,int start,int terminal)
	{
		this.netsize = net.getNetsize();
	this.start = start;
	this.terminal= terminal;
		this.rout = new ArrayList<Integer>();
		
		
		this.Nettopo = new topology(netsize);
		
		generate_topo_map(net);
		this.Nettopo.config(net);
		
		
		this.topology = Nettopo.get_topo();
	
				find_route_topo(net);
		
		transfer = new Transport(this,net);
		
		//initial();
	}
	
	private void generate_topo_map(NetWork net)
	{
		int c_id=0;
		for(int y = 0; y <netsize;y++)
		{
			c_id=0;
			for(int x = 0; x <netsize;x++)
			{	
				
				int dis = net.node(y).get_connection().get(c_id).getDistance();
				int order =net.node(y).get_connection().get(c_id).getTargetOrder();
				if(x==order)
				{
					Nettopo.update(y, x, dis);
					if(c_id<net.node(y).getsize()-1)
					{
					c_id++;
					}
				}
				else
				{
					Nettopo.update(y, x, 9);
				}
			}
			Nettopo.update(y, y, 0);
		}
		
	}
	
	public void find_route_topo(NetWork net)
	{
		boolean notfind = true;
		int counter=0;
		ArrayList<Integer>temp = new ArrayList<Integer>();
		int remember = 9;
		int anchor = 0;
		int firstnode = start;
		int lastnode = terminal;
		rout.add(start);
		do{
			
			for(int y=0;y<topology[start].length;y++)
			{
				if(topology[start][y]==1)
				{
					temp.add(y);
					/*System.out.println(y);
					System.out.println(start);
					System.out.println();*/
				}
			}
		
			for(int y=0;y<temp.size();y++)
			{
				
				if(remember>topology[temp.get(y)][terminal])
				{
					
				
					remember = topology[temp.get(y)][terminal];
					
					anchor = temp.get(y);
					
				}
				else if(topology[temp.get(y)][terminal]==1)
				{
					anchor = temp.get(y);
					notfind=false;
				}
			}
			start =anchor;
			rout.add(start);
			//System.out.println(start);
			temp.clear();
			
			if(notfind == false)
			{
				net.node(firstnode).set_node_connection(net.node(lastnode),rout.get(rout.size()-1));
				if(terminal != start)
				rout.add(terminal);
				break;
			}
			if(counter>9)
			{
				System.out.println("start node were not merge with terminal node");
				break;
			}
			counter++;
		}while(true);
		
	}
	public topology get_topo()
	{
		return Nettopo;
	}
	public ArrayList<Integer> get_rout()
	{
		return rout;
	}
	/*public void getroute()
	{
		for(int i : rout)
		{
		System.out.println(i);
		}
	}*/
	
}
