package simple;

import java.util.ArrayList;

public class NetNode {
	//cordinator
	private int order;
	private boolean availible;
	private int dataholder;
	private boolean confirmed;
	//connected node

	private ArrayList<Connection> connections;

	
	NetNode(int order)
	{
		connections = new ArrayList<Connection>();
		this.order = order;
		this.availible = true;
		
		this.confirmed = false;
		this.dataholder = 4000;
		
	}
	
	
	public int getDataholder() {
		return dataholder;
	}


	public void setDataholder(int dataholder) {
		this.dataholder += dataholder;
	}


	public void set_node_connection(NetNode targetnode, int distance)
	{
		int Via = targetnode.getOrder();
		connections.add(new Connection(targetnode.getOrder(),distance,Via));
	
	}
	
	public ArrayList<Connection> get_connection()
	{
		return connections;
	}
	public int getOrder(){return order;}
	
	public int getsize(){return connections.size();}

	public boolean isavailible() {
		return availible;
	}


	

	public boolean isconfirmed() {
		return confirmed;
	}
	public void notavailible() {
		if (availible)
		{
			availible=false;
		}
		else
		{
			availible=true;
		}
			
	}


	

	public void confirmation() {
		if (confirmed)
		{
			confirmed=false;
		}
		else
		{
			confirmed=true;
		}
	
	}

	public void print_node()
	{
		System.out.println("-----------------------------------");
		System.out.print("Node# "+ 
				Integer.toString(order)+ 
				"  | ");
		
		for(int i=0;i<connections.size();i++)
		System.out.print(
		Integer.toString(connections.get(i).getTargetOrder())+
		", "
		);
		System.out.println();
		System.out.print("Hops     | ");
		for(int i=0;i<connections.size();i++)
		{
			
			System.out.print(Integer.toString(connections.get(i).getDistance()) +", "
					);
			
		}
		System.out.println();
		System.out.print("Via      | ");
		for(int i=0;i<connections.size();i++)
		{
			
			System.out.print(Integer.toString(connections.get(i).getVia()) +", "
					);
			
		}
		System.out.println();
		

	}
	

}
